package com.unimelb.breakoutgame;

import java.util.LinkedList;

import com.unimelb.bouncyball.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

public class Ball {
	private WorldView worldView;
    private Bitmap bmp;
    Paint paint;
    float dRadius;
    
    public float dCenterX;
    public float dCenterY;
    float dxSpeed;
    float dySpeed;
    private int nScreenWidth;
    private int nScreenHeight;
    Paddle paddle;
    Context context;
    Bitmap bitmScaledImage;
    LinkedList<Brick> bricks;
    float dXSpeedConstant, dYSpeedConstant;
    private SoundPlayer player;
    private LinkedList<Bonus> bonuses;
    
    public Ball(Paddle paddle, WorldView worldView, Bitmap bmp, int nScreenWidth, int nScreenHeight, Context context, int nRadius, LinkedList<Brick> _bricks,
    		    int nPosX, int nPosY, int nSpeedX, int nSpeedY, SoundPlayer player, LinkedList<Bonus> bonuses) {
    	  this.bonuses = bonuses;
          this.worldView = worldView;
          this.context = context;
          this.bmp = bmp;
          this.paddle = paddle;
          this.nScreenWidth = nScreenWidth;
          this.nScreenHeight = nScreenHeight;
          dRadius = nScreenWidth* nRadius/100;
          this.bricks = _bricks;
          paint = new Paint();
      	  paint.setAntiAlias(true);
      	  paint.setColor(Color.RED);      	  
      	  dXSpeedConstant = nSpeedX * dRadius/100; 
      	  dYSpeedConstant = nSpeedY * dRadius/100;
      	  dxSpeed = dXSpeedConstant;
    	  dySpeed = dYSpeedConstant;
    	  this.player = player;
      	  //dCenterX = paddle.dCenter;
          //dCenterY = this.nScreenHeight - paddle.rec.height() - dRadius-1;
          dCenterX = nPosX * nScreenWidth/100;
          dCenterY = nPosY * nScreenHeight/100;
          updateCenter(dCenterX, dCenterY);
          
          Bitmap image = BitmapFactory.decodeResource(context.getResources(), R.drawable.ball);
  		  bitmScaledImage = Bitmap.createScaledBitmap(image, 2*(int)dRadius, 2*(int)dRadius, true);
    }

	public void updateCenter(float x, float y) {
    	this.dCenterX = x;
    	this.dCenterY = y;
    }
	
	public int bCollisionWithBrick(){
		int nDx=2;
		boolean bCollision = false;
		for (Brick brick : this.bricks) {
			//left collision
			if(dCenterX + dRadius <= brick.nLeft + nDx && dCenterX + dRadius >= brick.nLeft - nDx
			   && dCenterY <= brick.nTop + brick.nHeight && dCenterY >= brick.nTop){
				dxSpeed *=-1;
				bCollision = true;
			}
			//right collision
			else if(dCenterX - dRadius <= brick.nLeft + brick.nWidth + nDx && dCenterX - dRadius >= brick.nLeft + brick.nWidth - nDx
					&& dCenterY <= brick.nTop + brick.nHeight && dCenterY >= brick.nTop){
				dxSpeed *=-1;
				bCollision = true;
			}
			//bottom collision
			else if(dCenterX >= brick.nLeft && dCenterX <= brick.nLeft + brick.nWidth  &&
					dCenterY - dRadius <= brick.nTop+brick.nHeight+nDx && dCenterY-dRadius >= brick.nTop+brick.nHeight-nDx){
				dySpeed *=-1;
				bCollision = true;
			}
			//top collision
			else if(dCenterX <= brick.nLeft + brick.nWidth && dCenterX >= brick.nLeft &&
					dCenterY + dRadius <= brick.nTop+nDx&& dCenterY+dRadius >= brick.nTop-nDx){
				dySpeed *=-1;
				bCollision = true;
			}	
			//bottom-left checking
			else if(dCenterX >= brick.nLeft && dCenterX <= brick.nLeft+brick.nWidth/2
					&& dCenterY >= brick.nTop+brick.nHeight/2 && dCenterY <= brick.nTop+brick.nHeight){
				dxSpeed*=-1;
				dySpeed*=-1;
				bCollision=true;
			}
			//bottom-right checking
			else if(dCenterX >= brick.nLeft+brick.nWidth/2 && dCenterX <= brick.nLeft+brick.nWidth
					&& dCenterY >= brick.nTop+brick.nHeight/2 && dCenterY <= brick.nTop+brick.nHeight){
				dxSpeed*=-1;
				dySpeed*=-1;
				bCollision=true;
			}
			//top-left checking
			else if(dCenterX >= brick.nLeft && dCenterX <= brick.nLeft+brick.nWidth/2
					&& dCenterY >= brick.nTop && dCenterY <= brick.nTop+brick.nHeight/2){
				dxSpeed*=-1;
				dySpeed*=-1;
				bCollision=true;
			}
			//top-right checking
			else if(dCenterX >= brick.nLeft+brick.nWidth/2 && dCenterX <= brick.nLeft+brick.nWidth
					&& dCenterY >= brick.nTop && dCenterY <= brick.nTop+brick.nHeight/2){
				dxSpeed*=-1;
				dySpeed*=-1;
				bCollision=true;
			}
			if(bCollision){
				int nType = brick.nBonus;// -1 or 0 or 1
				if(nType != -1){
					if(nType == 1){
						nType = (Math.random() < 0.5)? 1:2;
					}
					bonuses.add(new Bonus(brick.nLeft + brick.nWidth/2, brick.nTop + brick.nHeight, this, context, paddle, nScreenWidth, nScreenHeight, nType));
				}
				
				int nScore = brick.nScore;
				bricks.remove(brick);	
				dCenterX = dCenterX + dxSpeed;
				dCenterY = dCenterY + dySpeed;
				
				player.brickSound();
				
				return nScore;
			}			
		}		
		return -1;
	}
	public boolean bGameOver(){
		if(dCenterY - dRadius > nScreenHeight)
			return true;
		return false;
	}
	public boolean bCollisionWithPaddle(){
		if(dCenterX <= paddle.dCenter + paddle.nWidth/2 + dRadius && dCenterX >= paddle.dCenter - paddle.nWidth / 2 - dRadius){
			if(dCenterY <= nScreenHeight - paddle.nHeight && dCenterY >= nScreenHeight - paddle.nHeight - dRadius){
				player.paddleSound();
				return true;
			}
		}		
		return false;
	}
	
	// return:
	// score - if collision with brick
	// -1 - if game over
	// 0 - otherwise	
	public int moveBall() {
		int nScore = bCollisionWithBrick();//-1 or score
		
		if(nScore != -1){
			return nScore;
		}		
		else if(bGameOver()){			
			return -1;
		}
		else if(bCollisionWithPaddle()){
			int nS1 = (int) (paddle.dCenter - paddle.nWidth/2 - dRadius);
			int nS2 = (int) (paddle.dCenter - paddle.nWidth/4);
			int nS3 = (int) (paddle.dCenter);
			int nS4 = (int) (paddle.dCenter + paddle.nWidth/4);
			int nS5 = (int) (paddle.dCenter + paddle.nWidth/2 + dRadius);
			
			if(dCenterX >= nS1 && dCenterX < nS2){
				dxSpeed = - (Math.abs(dXSpeedConstant) + Math.abs(dXSpeedConstant)/2) ;
				dySpeed *= -1;
			}
			else if(dCenterX >= nS2 && dCenterX < nS3){
				dxSpeed = - Math.abs(dXSpeedConstant);
				dySpeed *= -1;
			}
			else if(dCenterX >= nS3 && dCenterX < nS4){
				dxSpeed = Math.abs(dXSpeedConstant);
				dySpeed *= -1;
			}
			else if(dCenterX >= nS4 && dCenterX < nS5){
				dxSpeed = (Math.abs(dXSpeedConstant) + Math.abs(dXSpeedConstant)/2);
				dySpeed *= -1;
			}
			
			Log.d("Android:","collision");			
		}
		else{
			if(dCenterX + dRadius >= nScreenWidth){
				dxSpeed = - dxSpeed;
			}
			if(dCenterX - dRadius <= 0){
				dxSpeed = - dxSpeed;
			}
			if(dCenterY - dRadius <= 0){
				dySpeed = -dySpeed;
			}							
		}		
		
		dCenterX = dCenterX + dxSpeed;
		dCenterY = dCenterY + dySpeed;
		return 0;
	}
	
    public int draw(Canvas canvas) {
    	int nResult = 0;
    	if(worldView.bOnScreen) {
    		nResult = moveBall();    		
    		updateCenter(dCenterX, dCenterY);    		    		
    		//canvas.drawCircle(dCenterX, dCenterY, dRadius, paint);
    		canvas.drawBitmap(bitmScaledImage, dCenterX-dRadius, dCenterY-dRadius, null);
    	}
    	return nResult;
    }
}