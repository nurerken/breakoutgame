package com.unimelb.breakoutgame;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import com.parse.Parse;
import com.unimelb.bouncyball.R;

import android.os.Bundle;
import android.os.Environment;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends Activity {
	private WorldView worldView;
	private DataManager dataManager = new DataManager();
	private PersonalInformation personal = new PersonalInformation();
	public SoundPlayer player = new SoundPlayer();
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		//SoundPlayer player = new SoundPlayer();

//		player.inTop10Sound();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		player.setApplause(R.string.ApplauseSound);
		player.setDreamWedding(R.string.DreamWedding);
		player.setBrickFileName(R.string.brickSound);
		player.setPaddleFileName(R.string.PaddleSound);
		player.setOK(R.string.OK);	
		
		player.setnBallSpeed(R.string.Ball);
		player.setnExtraLife(R.string.ExtraLife);
		player.setnGameOver(R.string.GameOver);
		player.setnPaddle(R.string.PaddleIncrease);
		player.setnStageComplete(R.string.StageComplete);
		
		player.setContext(this);
		player.backgroundSound();
		/*File wallpaperDirectory = new File("/"+Environment.getExternalStorageDirectory().getPath() +"/bricks/");
        // have the object build the directory structure, if needed.
		if(!wallpaperDirectory.exists())
		{
			 wallpaperDirectory.mkdirs();			 
			 InputStream in;
			try {
				in = getAssets().open("levels.xml");
				copy(in , new File("/"+Environment.getExternalStorageDirectory().getPath() +"/bricks/levels.xml"));
				
				in = getAssets().open("oikos.xml");
				copy(in , new File("/"+Environment.getExternalStorageDirectory().getPath() +"/bricks/oikos.xml"));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
		}*/
       

		//setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); //Force device to stay in portrait orientation
		requestWindowFeature(Window.FEATURE_NO_TITLE); //Remove banner from the top of the activity
		setContentView(R.layout.main_menu); //Set the layout to activity_main
		onInitializeParse();
	    //player.backgroundSound();
		//player.brickSound();
		
	}
	
	public void copy(InputStream in, File dst) throws IOException {
	    //InputStream in = new FileInputStream(src);
	    OutputStream out = new FileOutputStream(dst);

	    byte[] buf = new byte[1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
	        out.write(buf, 0, len);
	    }
	    in.close();
	    out.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/** Called just before the activity is destroyed. */
    @Override public void onDestroy() {
    	super.onDestroy(); 
    	 
    }
    
	public void onInitializeParse(){
		Parse.initialize(this, "CrTWTEmzhM4qDU3doS21ETcjXhxIY4CKTOIYHobz", "Lf8fll7ktEATtEIMIXXMUSlECBpz2FeSO0iKFPRJ");
		PersonalInformation personal = new PersonalInformation();
		dataManager.setAccount(personal.getAccountInformation((AccountManager)getSystemService(ACCOUNT_SERVICE)));
		//((DataManager)this.getApplication()).setAccount(personal.getAccountInformation((AccountManager)getSystemService(ACCOUNT_SERVICE)));
	}
	/*
	public boolean checkTop10(int score){
		return false;//if score>=T10
	}
	
	public boolean UpdateTop10(String email, int score, String msg){
		//update database table
		  
	}
	*/
	public void newGame(View view)
	{
		setContentView(R.layout.activity_main); //Set the layout to activity_main
	
		worldView = (WorldView) findViewById(R.id.worldView);
	
		worldView.setPlayerName(personal.getUserName(this.getContentResolver()));
		worldView.setAccountName(personal.getAccountInformation((AccountManager)getSystemService(ACCOUNT_SERVICE)));
		
		
		/*setContentView(R.layout.activity_main); //Set the layout to activity_main

		worldView = (WorldView) findViewById(R.id.worldView);*/	
	}
	
public void bestPlayers(View view)
		{
			setContentView(R.layout.best_scores);
			/*progressDialog=new ProgressDialog(this);
			progressDialog.setMessage("Loading, please wait...");
			progressDialog.show();*/
			
			ProgressBar progressBar;
			//ListView ll;
			progressBar=(ProgressBar) findViewById(R.layout.best_scores);

			String[] topScores = new String[10];

			
			String txt="";
			int i=0;
			List<Top10Entities> list = new Parse_Functions().getTop10List(1, getResources().getString(R.string.Parse_Tables_Top10));
			for (Top10Entities top10Entities : list) {
				txt=Integer.toString(top10Entities.getOrder())+"	"+top10Entities.getDisplayName()+"	"+top10Entities.getScore()+"\n";
				topScores[i]=txt;
				i++;
			}
			
			//progressBar.setVisibility(0);
			
			/*ll=(ListView) findViewById(R.layout.best_scores);
			ll.setVisibility(1);*/
			
			
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,	android.R.layout.simple_list_item_1, topScores);
			ListView lv = (ListView)findViewById(R.id.best_scores_txt);	
			lv.setAdapter(adapter);
			//progressDialog.hide();
			
			
		}

	
	public void downloadLevel(View view) throws IOException
	{
		setContentView(R.layout.download_level);
	  final Baidu downloadTask = new Baidu(MainActivity.this);
	   downloadTask.execute("http://bcs.duapp.com/structuresoflevel/nomos.xml");
	   findViewById(R.id.loading).setVisibility(View.GONE);
	   
	   AlertDialog.Builder dialog= new AlertDialog.Builder(this);
	   dialog.setTitle("Download Complete");
	   dialog.setMessage("Level Nomos has been successfully downloaded");
	   dialog.setCancelable(false);
	   dialog.setNegativeButton("OK",
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
	   
	   XmlReader.addLevelToList("nomos", getApplicationContext());
	   AlertDialog alert=dialog.create();
	   alert.show();
	
	}
	
	public void help(View view)
	{
		setContentView(R.layout.help);
	}
	
	public void exit(View view)
	{
		this.exit(view);
	}
	
	public void back(View view)
	{
		setContentView(R.layout.main_menu);
	}
}
