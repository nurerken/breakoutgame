package com.unimelb.breakoutgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.unimelb.bouncyball.R;

public class Bonus {
	int nX, nY;
	double dRadius;
	double dSpeed;
	Bitmap bitmScaledImage;
	Paddle paddle;
	int nScreenHeight, nScreenWidth;
	int nType; // 0 - pill, 1 - increase paddle width, 2 - increase ball speed
	
	public Bonus(int nX, int nY, Ball ball, Context context, Paddle paddle, int nScrW, int nScrH, int nType){
		this.nX = nX;
		this.nY = nY;
		this.dRadius = ball.dRadius;
		this.nScreenHeight = nScrH;
		this.nScreenWidth = nScrW;
		dSpeed = dRadius * 0.3;
		this.paddle = paddle;
		int nID = nType == 0 ? R.drawable.pill:R.drawable.question;
		Bitmap image = BitmapFactory.decodeResource(context.getResources(), nID);
		bitmScaledImage = Bitmap.createScaledBitmap(image, 2*(int)dRadius, 2*(int)dRadius, true);
		this.nType = nType;
	}	
	
	public boolean bCollisionWithPaddle(){
		if(nX <= paddle.dCenter + paddle.nWidth/2 + dRadius && nX >= paddle.dCenter - paddle.nWidth / 2 - dRadius){
			if(nY <= nScreenHeight - paddle.nHeight && nY >= nScreenHeight - paddle.nHeight - dRadius){
				return true;
			}
		}		
		return false;
	}
	
	public void move(){		
		nY += dSpeed;		
	}
	
	public boolean draw(Canvas canvas) {
		move();
    	canvas.drawBitmap(bitmScaledImage, (float)(nX-dRadius), (float)(nY-dRadius), null);
    	
    	return bCollisionWithPaddle();
    }
}
