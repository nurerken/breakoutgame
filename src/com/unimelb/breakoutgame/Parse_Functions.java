package com.unimelb.breakoutgame;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;


public class Parse_Functions {

	
	public boolean ValidAccount(String account,String tableName){
//		final boolean[] isRegistered = new boolean[0];
		 boolean[] isRegistered = new boolean[1];
		ParseQuery<ParseObject> query = ParseQuery.getQuery(tableName);
		query.whereEqualTo("UserName", account);
		try {
			isRegistered[0] =query.find().isEmpty();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				
		return isRegistered[0];
		
		
	}
	
	public void CreatAccount(String account,String tableName){
		ParseObject insert = new ParseObject(tableName);
		insert.put("UserName", account);
		insert.put("CurrentLevel", 1);
		insert.saveInBackground();
	}
	/*
	public boolean checkTop10(int level,int score,String tableName){
		boolean isTop10 = false;
		ParseQuery<ParseObject> query = ParseQuery.getQuery(tableName);
		try {
			 int hasTop10 = query.whereEqualTo("Level", level).count();
			 if(hasTop10<11){	
				 isTop10 = true;
			 }else{
				 isTop10 = query.whereEqualTo("Level",level).whereEqualTo("order", 10).whereGreaterThan("score", score).count()>0? true:false;
				
				 
			 }
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return isTop10;
		
	
		
	}
	*/
	
	public boolean checkTop10(int score,String accountName ,StringBuilder errorMessage){
		boolean isTop10 = false;
			 List<ParseObject> hasTop10;
			try {
				ParseQuery<ParseObject> query = ParseQuery.getQuery("TopScoreList");				
				hasTop10 = query.whereEqualTo("Level", 1).find();
				
				 if(hasTop10.size()<10){	
					 isTop10 = true;
				 }else{
					 					 
					 boolean isAccountNameInDataBase = false;
					 boolean isHigherThanAnyOne = false;
					 for (ParseObject parseObject : hasTop10) {
						 String a = parseObject.getString("AccountName");
						 int localScore = parseObject.getNumber("Score").intValue();
						    if(parseObject.getString("AccountName").equalsIgnoreCase(accountName)){
						    	 isTop10 = score >parseObject.getNumber("Score").intValue()? true:false;
						    	isAccountNameInDataBase = true;
						    }else if(score>localScore){
						    	isHigherThanAnyOne = true;
						    	
						    }
						}
					 

					 if(isHigherThanAnyOne && !isAccountNameInDataBase){
						 isTop10 = true;
					 }
					   
					
				 }
			
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				if(e!=null){
					if(e.getCode()==e.CONNECTION_FAILED){
					  errorMessage.append("No Internet Connection");
					}else {
						errorMessage.append(e.getMessage().toString());
					}
				
				}else{
				  errorMessage.append("No Internet Connection");
				}
			}
			
		
		return isTop10;
		
	
		
	}
	
	/*
	public boolean updateTop10(int level,int score,String accountName,String tableName,String errorMessage){
		boolean isSccess = false;
		final int userScore = score;
		try {
			ParseQuery<ParseObject> delete = ParseQuery.getQuery(tableName);
			delete.addAscendingOrder("Score").setLimit(1).findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					// TODO Auto-generated method stub
					for (ParseObject parseObject : objects) {
						 ParseObject.createWithoutData("TopScoreList", parseObject.getString("objectId")).deleteEventually();
					}
				}
			});
			
			
		    ParseObject insert = new ParseObject(tableName);
		    insert.put("AccountName", accountName);
		    insert.put("Level",level);
		    insert.put("Score",score);
		    insert.saveInBackground();
		    isSccess = true;
		} catch (Exception e) {
			// TODO: handle exception
			errorMessage =e.getMessage().toString();
		}
		
		return isSccess;
		
		
	}
	*/
	
	
	public boolean updateTop10(int score,String accountName,String displayName,String errorMessage)
	{
		boolean isSccess = false;
		final int userScore = score;
		try {
			ParseQuery<ParseObject> list = ParseQuery.getQuery("TopScoreList");
			List<ParseObject> deleteList = list.find(); 
			
			if(isUserInTop10(deleteList, accountName)){
				updateData(deleteList, score, accountName);
			}else if(deleteList.size()>=10){
				deleteLast1Data(accountName);
				insertDataToTopList(accountName,displayName, 1, score);
			}else {
				insertDataToTopList(accountName,displayName,1, score);
			}
											
		    isSccess = true;
		} catch (ParseException e) {
			// TODO: handle exception
			if(e!=null){
				errorMessage = e.getMessage().toString();
				}else{
				  errorMessage = "No Internet Connection";
				}
		}
		
		return isSccess;
		
		
	}
	
	
	
	public boolean isUserInTop10(List<ParseObject> list,String accountName){
		boolean inTop10 = false;
	    for (ParseObject parseObject : list) {
	    
	    	if(accountName.equalsIgnoreCase(parseObject.getString("AccountName"))){
	    		inTop10 = true;
	    		break;
	    	}
			//parseObject.getString("AccountName").toString().equals? true: false;
			
		}
		
	    return inTop10;
	}
	
	
	
	public void deleteLast1Data(String accountName) throws ParseException{
		ParseQuery<ParseObject> delete = ParseQuery.getQuery("TopScoreList");
		//String msg = Integer.toString(delete.whereEqualTo("AccountName",accountName).count());
		//Log.d("deletecount", msg);
		
		  delete.addAscendingOrder("Score").setLimit(1).find().remove(0).delete();
			/*delete.addAscendingOrder("Score").setLimit(1).findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					// TODO Auto-generated method stub
					for (ParseObject parseObject : objects) {
						 ParseObject.createWithoutData("TopScoreList", parseObject.getString("objectId")).deleteEventually();
					}
				}
			});
			*/
		
	}
	
	
	public void updateData(List<ParseObject> list, int score,String accountName) throws ParseException{
		  for (ParseObject parseObject : list) {
			  if(accountName.equalsIgnoreCase(parseObject.getString("AccountName"))){
		    		parseObject.put("Score", score);
		    		parseObject.saveEventually();
		    	}
		}
	}
	
	public void insertDataToTopList(String accountName,String displayName,int level,int score) throws ParseException{
		 ParseObject insert = new ParseObject("TopScoreList");		  
		    insert.put("AccountName", accountName);
		    insert.put("PlayerName", displayName);
		    insert.put("Level",level);
		    insert.put("Score",score);
		    insert.saveInBackground();
	}
	
	
	public List<Top10Entities> getTop10List(int level,String tableName){
		ParseQuery<ParseObject> query = ParseQuery.getQuery(tableName);
		List<Top10Entities> entities = new ArrayList<Top10Entities>();
		try {
			List<ParseObject> list = query.whereEqualTo("Level", level).addDescendingOrder("Score").find();
			for (ParseObject objects : list) {
		      Top10Entities data = new Top10Entities();
		      data.setAccountName(objects.getString("AccountName"));
		      data.setDisplayName(objects.getString("PlayerName"));
		      data.setOrder(entities.size()+1);
		      data.setLevel(objects.getNumber("Level").intValue());
		      data.setScore(objects.getNumber("Score").intValue());
		      entities.add(data);
			} 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return entities;
	}
	
	
	
	
}
