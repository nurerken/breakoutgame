package com.unimelb.breakoutgame;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;

import android.R;
import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources.NotFoundException;
import android.media.MediaPlayer;
import android.os.Environment;

public class SoundPlayer{
	Context context;
	int brickFileName;
    int paddleFileName;
    int applause;
    int dreamWedding;
    int nOK;
    int nBallSpeed;
	int nExtraLife;
    int nGameOver;
    int nPaddle;
    int nStageComplete;
    
    public int getnOK() {
		return nOK;
	}
	public void setnOK(int nOK) {
		this.nOK = nOK;
	}
	public int getnBallSpeed() {
		return nBallSpeed;
	}
	public void setnBallSpeed(int nBallSpeed) {
		this.nBallSpeed = nBallSpeed;
	}
	public int getnExtraLife() {
		return nExtraLife;
	}
	public void setnExtraLife(int nExtraLife) {
		this.nExtraLife = nExtraLife;
	}
	public int getnGameOver() {
		return nGameOver;
	}
	public void setnGameOver(int nGameOver) {
		this.nGameOver = nGameOver;
	}
	public int getnPaddle() {
		return nPaddle;
	}
	public void setnPaddle(int nPaddle) {
		this.nPaddle = nPaddle;
	}
	public int getnStageComplete() {
		return nStageComplete;
	}
	public void setnStageComplete(int nStageComplete) {
		this.nStageComplete = nStageComplete;
	}
    
    public Context getContext() {
		return context;
	}
	public void setContext(Context context) {
		this.context = context;
	}
	public int getBrickFileName() {
		return brickFileName;
	}
	public void setBrickFileName(int brickFileName) {
		this.brickFileName = brickFileName;
	}
	public int getOK() {
		return nOK;
	}
	public void setOK(int nOK) {
		this.nOK = nOK;
	}
	public int getPaddleFileName() {
		return paddleFileName;
	}
	public void setPaddleFileName(int paddleFileName) {
		this.paddleFileName = paddleFileName;
	}
	public int getApplause() {
		return applause;
	}
	public void setApplause(int applause) {
		this.applause = applause;
	}
	public int getDreamWedding() {
		return dreamWedding;
	}
	public void setDreamWedding(int dreamWedding) {
		this.dreamWedding = dreamWedding;
	}


	public void stageCompleteSound()  {
		try {
			AssetFileDescriptor file = context.getAssets().openFd(context.getResources().getString(nStageComplete));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
				       
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.start();		
			
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void paddleIncreaseSound()  {
		try {
			AssetFileDescriptor file = context.getAssets().openFd(context.getResources().getString(nPaddle));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
				       
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.start();		
			
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void gameOverSound()  {
		try {
			AssetFileDescriptor file = context.getAssets().openFd(context.getResources().getString(nGameOver));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
				       
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.start();		
			
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void extralifeSound()  {
		try {
			AssetFileDescriptor file = context.getAssets().openFd(context.getResources().getString(nExtraLife));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
				       
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.start();		
			
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void BallSpeedSound()  {
		try {
			AssetFileDescriptor file = context.getAssets().openFd(context.getResources().getString(nBallSpeed));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
				       
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.start();		
			
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void OKSound()  {
		try {
			AssetFileDescriptor file = context.getAssets().openFd(context.getResources().getString(nOK));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
				       
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.start();
		
			
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void brickSound()  {
		try {
			AssetFileDescriptor file = context.getAssets().openFd(context.getResources().getString(brickFileName));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
				       
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.start();
		
			
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void inTop10Sound(){
		AssetFileDescriptor file;
		try {
			file = context.getAssets().openFd(context.getResources().getString(applause));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.start();

		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void backgroundSound(){
		AssetFileDescriptor file;
		try {
			file = context.getAssets().openFd(context.getResources().getString(dreamWedding));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.setLooping(true);
			media.start();
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void paddleSound(){
		AssetFileDescriptor file;
		try {
			file = context.getAssets().openFd(context.getResources().getString(paddleFileName));
			FileDescriptor fd = file.getFileDescriptor();
			MediaPlayer media = new MediaPlayer();
			media.setDataSource(fd,file.getStartOffset(),file.getLength());
			media.prepare();
			media.start();
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
