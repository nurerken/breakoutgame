package com.unimelb.breakoutgame;

import com.unimelb.bouncyball.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.widget.TextView;

public class Brick {
	int nLeft;
	int nTop;
	int nWidth;
	int nHeight;
	int nType;// 0: green/100; 1: blue/200; 2: red/300;
	int nScore;
	Context context;
	Bitmap bitmScaledImage;
	WorldView worldView;
	int nBonus;
	
	public Brick(WorldView worldView, int nScreenWidth, int nScreenHeight, int nLeft, int nTop, int nWidth, int nHeight, int nType, Context context, int nBouns){
		this.worldView = worldView;
		this.nLeft = nLeft*nScreenWidth/100;
		this.nTop = nTop*nScreenHeight/100;
		this.nWidth = nWidth*nScreenWidth/100;
		this.nHeight = nHeight*nScreenHeight/100;
		this.nType = nType;
		Bitmap image=null;
		this.nBonus = nBouns;
		
		if(nType == 1){
			//nColor = Color.GREEN;
			nScore = 100;
			image = BitmapFactory.decodeResource(context.getResources(), R.drawable.brick_green);
		}
		else if(nType == 2){
			//nColor = Color.rgb(148, 0, 211);
			nScore = 200;
			image = BitmapFactory.decodeResource(context.getResources(), R.drawable.brick_blue);
		}
		else if(nType == 3){
			//nColor = Color.RED;
			nScore = 300;
			image = BitmapFactory.decodeResource(context.getResources(), R.drawable.brick_red);
		}
		if(image!=null)		
			bitmScaledImage = Bitmap.createScaledBitmap(image, this.nWidth, this.nHeight, true);
		else{
			System.out.println();
		}		
	}
	
	void draw(Canvas canvas){
		if(worldView.bOnScreen) {
			canvas.drawBitmap(bitmScaledImage, this.nLeft, this.nTop, null);	
		}		
	}
}
