package com.unimelb.breakoutgame;

import android.util.Patterns;

import java.util.regex.Pattern;

/**
 * Created by tuhung-te on 12/09/2014.
 */
public class Regu_Express {
    private static String mailPattern  = "^[a-z0-9](\\.?[a-z0-9]){5,}@g(oogle)?mail\\.com$";

    public  boolean  validMail(String mailString) {
        Pattern validPattern = Pattern.compile(mailPattern);
        return validPattern.matcher(mailString).find()? true:false;
        //return Patterns.EMAIL_ADDRESS.matcher(mailString).matches();
    }
}
