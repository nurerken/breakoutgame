package com.unimelb.breakoutgame;

import java.text.BreakIterator;
import java.util.LinkedList;

import com.unimelb.bouncyball.R;

import android.animation.AnimatorSet.Builder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class WorldView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
	private SurfaceHolder surfaceHolder;
	private boolean bRunning = false;
	public Ball ball;
	public Paddle paddle;
	public boolean bOnScreen = true;
	private boolean bTouched = false;
	private int nWidth;
	private int nHeight;
	private Bitmap bitmScaledImage;
	private float dEventX;
    private String accountName;
    private boolean bUpdate = false;
    private boolean bOk = false;
    private boolean bOk2 = false;
    private boolean bSuccess = false;
    private Parse_Functions parse = new Parse_Functions();
    private String sErr;
    

	private LinkedList<Brick> bricks;
	private LinkedList<Bonus> bonuses;
	Context context;
	TextView textViewLevel;
	TextView textViewScore;
	TextView textViewLife;
	TextView textViewLabel;
	boolean bExit = false;
		
	private boolean bStarted = false;
	private boolean bballLost = false;
	private boolean bGameOver = false;
	private String PlayerName;
	private int nLevel = 0;
	private int nScore = 0;
	private int nPlayerLife = 3;
	
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	public String getPlayerName() {
		return PlayerName;
	}

	public void setPlayerName(String playerName) {
		PlayerName = playerName;
	}
    		
	public WorldView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		
		getHolder().addCallback(this);
		setFocusable(true);
	 				
	}
		
	private void setUserName(){
		TextView txtView = (TextView) ((Activity)context).findViewById(R.id.textView1);
		txtView.setText("Player: "+PlayerName);
	}
			
	@SuppressLint("WrongCall")
	public void run() {
		
		while(bRunning && !bGameOver) {
			if(!bStarted)
				continue;
			
			if (bTouched) {
				paddle.move((int) dEventX);
				bTouched = false;								
			}
			
			Canvas canvas = null;
			try {
				canvas = surfaceHolder.lockCanvas(null);
				synchronized(surfaceHolder) {
					
					onDraw(canvas);
					paddle.draw(canvas);
					int nResult = ball.draw(canvas);
					
					for (int i = 0; i < bricks.size(); i++){
						bricks.get(i).draw(canvas);						
					}
					
					for (int i = 0; i < bonuses.size(); i++){
						boolean bCollision = bonuses.get(i).draw(canvas);	
						
						if(bCollision){
							int nType = bonuses.get(i).nType;
							if(nType == 0){
								nPlayerLife++;
								((MainActivity)context).player.extralifeSound();
								((Activity)context).runOnUiThread(new Runnable() {
								     @Override
								     public void run() {
								     textViewLife.setText("Life: " + nPlayerLife);									     
								     }
								});		
							}
							else if (nType == 1){
								((MainActivity)context).player.paddleIncreaseSound();
								paddle.IncreasePaddleWidth();
							}
							else if (nType == 2){
								((MainActivity)context).player.BallSpeedSound();
								ball.dxSpeed += ball.dxSpeed*0.3;
								ball.dySpeed += ball.dySpeed*0.3;
								ball.dXSpeedConstant += ball.dXSpeedConstant*0.3;
								ball.dYSpeedConstant+= ball.dYSpeedConstant*0.3;
							}
							bonuses.remove(bonuses.get(i));
						}
					}
					
					if(nResult == -1){// ball went down
						bballLost = true;
					}
					else if (nResult == 0){//nothing..						
					}
					else{//collision with brick
						nScore += nResult;
						((Activity)context).runOnUiThread(new Runnable() {
						     @Override
						     public void run() {
						     textViewScore.setText("Score: " + nScore);	
						     }
						});		
					}
				}
        	} finally {
        		if (canvas != null) {
        			surfaceHolder.unlockCanvasAndPost(canvas);
        		}
        	}
			try {
				Thread.sleep(5);
			} catch(Exception e) {}
			
			if(bballLost)
				ballLost();
			
			if(bGameOver){
				gameOver();
			}
			else if(bricks.size()==0){
				nextLevel();
			}
		}				
	}
	
	ProgressDialog pd;
	private void gameOver(){
		((MainActivity)context).player.gameOverSound();
		bOk2 = false;
		((Activity)context).runOnUiThread(new Runnable() {
 		     @Override
 		     public void run() {
 		    	textViewLabel.setVisibility(View.VISIBLE);
 		    	textViewLabel.setText("Game Over");
 		    	bOk2 = true; 		    	
 		     }
 		   });
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while(!bOk2){
			//wait for thread
			
		}
		//update high score if necessary
		//start again or return to main menu
		checkHighScore();
	    //return to main menu
	    ((Activity)context).runOnUiThread(new Runnable() {
  		     @Override
  		     public void run() {
  		    	((MainActivity)context).setContentView(R.layout.main_menu);
  		     }
  		   });	    
	}
	
	private boolean bCheckHighScoreFinished;
	private void checkHighScore(){
		
		StringBuilder sErrorMsg = new StringBuilder();
	    
		((Activity)context).runOnUiThread(new Runnable() {
 		     @Override
 		     public void run() {
 		    	pd  = new ProgressDialog(context);
 		    	pd.setMessage("Checking for high score. Please wait..");
 		    	pd.show(); 		    	
 		     }
 		   });
				
	    boolean bIsHighScore = parse.checkTop10(nScore, accountName, sErrorMsg);
	    
	    ((Activity)context).runOnUiThread(new Runnable() {
		     @Override
		     public void run() {
		    	pd.hide();
		     }
		   });
	    
	    sErr = sErrorMsg.toString();
	    if(sErr.length() == 0){
	    	if(bIsHighScore){
	    		// ask user if he wants update high score
	    		// if yes then update
	    		bOk = false;
	    		((Activity)context).runOnUiThread(new Runnable() {
	   		     @Override
	   		     public void run() {
	   		    	bUpdate = false;
	 	    		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	 	    		    @Override
	 	    		    public void onClick(DialogInterface dialog, int which) {
	 	    		    	bOk = true;
	 	    		        switch (which){
	 	    		        case DialogInterface.BUTTON_POSITIVE:
	 	    		            //Yes button clicked
	 	    		        	bUpdate = true;
	 	    		            break;
	                         
	 	    		        case DialogInterface.BUTTON_NEGATIVE:
	 	    		            //No button clicked
	 	    		        	bUpdate = false;
	 	    		            break;
	 	    		        }
	 	    		    }
	 	    		    
	 	    		};
	 	    		
	 	    		if(true){
	 	    			((MainActivity)context).player.inTop10Sound();
	 	    			AlertDialog.Builder builder = new AlertDialog.Builder(context);
	 	    			builder.setMessage("Congratulations! Your score "+nScore+ "is in top 10 high scores. Do you wish to update high score table?").setPositiveButton("Yes", dialogClickListener)
	 	    				.setNegativeButton("No", dialogClickListener).show();
	 	    		}
	   		     }
	    		});
	    		

 	    		while(!bOk){
 	    			//wait ui thread
 	    		}
 	    		
	    		if(bOk && bUpdate){
	    			bOk2 = false;
	    			((Activity)context).runOnUiThread(new Runnable() {
	    	  		     @Override
	    	  		     public void run() {
	    	  		    	//Toast.makeText(context, "Updating..", Toast.LENGTH_LONG).show();
	    	  		    	textViewLabel.setVisibility(View.VISIBLE);
	    	  		    	textViewLabel.setText("Updating database. Please wait..");
	    	  		    	bOk2 = true;
	    	  		     }
	    	  		   });
	    			
	    			bSuccess = parse.updateTop10(nScore, accountName,PlayerName, sErr);
	    			
	    			while(!bOk2){
	    				//wait for ui thread
	    			}
	    			bOk2 = false;
	    			((Activity)context).runOnUiThread(new Runnable() {
	    	  		     @Override
	    	  		     public void run() {
	    	  		    	//Toast.makeText(context, "Updating..", Toast.LENGTH_LONG).show();
	    	  		    	textViewLabel.setVisibility(View.INVISIBLE);
	    	  		    	bOk2 = true;
	    	  		     }
	    	  		   });
	    			
	    			while(!bOk2){
	    				//wait for ui thread
	    			}
	    			
		    		if(!bSuccess){
		    			//show sErr and try again
		    			
		    		}
		    		else {
		    			//msg succesfully updated
		    			((Activity)context).runOnUiThread(new Runnable() {
		    	  		     @Override
		    	  		     public void run() {
		    	  		    	android.app.AlertDialog.Builder alert = new AlertDialog.Builder(context);
				    			alert.setTitle("Game");
				    			alert.setMessage("Your score has been successfully updated");
				    			alert.setPositiveButton("OK",null);
				    			alert.show();
		    	  		     }
		    	  		   });		    			
		    		}
	    		}	    			    		
	    	}	    	
	    }
	    else{
	    	bOk2 = false;
			((Activity)context).runOnUiThread(new Runnable() {
	  		     @Override
	  		     public void run() {
	  		    	//Toast.makeText(context, "Updating..", Toast.LENGTH_LONG).show();
	  		    	//pd.hide();
	  		    	textViewLabel.setVisibility(View.VISIBLE);
	  		    	textViewLabel.setText(sErr);	  		    	
	  		    	bOk2 = true;
	  		     }
	  		   });
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }	    
	    

		((Activity)context).runOnUiThread(new Runnable() {
 		     @Override
 		     public void run() {
 		    	pd.hide();
 		     }
 		   });
		bCheckHighScoreFinished = true;
	}
	
	
	private void ballLost(){
		if(--nPlayerLife == 0){
			bGameOver = true;
			return;
		}
		
		int nPaddleWidth = XmlReader.getPaddleWidth(nLevel, getContext());
		int nPaddleHeight = XmlReader.getPaddleHeight(nLevel, getContext());
		paddle = new Paddle(this, null, nWidth, nHeight, getContext(), nPaddleWidth, nPaddleHeight);
		
		int nRadius = XmlReader.getBallRadius(nLevel, getContext());
		int nBallPosX = XmlReader.getBallPosition_x(nLevel, getContext());
		int nBallPosY = XmlReader.getBallPosition_y(nLevel, getContext());
		int nBallSpeedX = XmlReader.getBallDirection_x(nLevel, getContext());
		int nBallSpeedY = XmlReader.getBallDirection_y(nLevel, getContext());
		
		ball = new Ball(paddle, this, null, nWidth, nHeight, getContext(), nRadius, bricks, nBallPosX, nBallPosY, nBallSpeedX, nBallSpeedY, ((MainActivity)context).player, bonuses);		
		
		bStarted = false;
		
		Canvas canvas = null;
		try {
			canvas = surfaceHolder.lockCanvas(null);
			synchronized(surfaceHolder) {
			    
				drawbackground(canvas);
				paddle.draw(canvas);
				ball.draw(canvas);
				for (int i = 0; i < bricks.size(); i++){
					bricks.get(i).draw(canvas);						
				}								
			}
    	} finally {
    		if (canvas != null) {
    			surfaceHolder.unlockCanvasAndPost(canvas);
    		}
    	}
		((Activity)context).runOnUiThread(new Runnable() {
		     @Override
		     public void run() {
		     textViewLife.setText("Life: " + nPlayerLife);	
		     }
		});		
		bballLost = false;
	}
	
	private void nextLevel(){
		nLevel++; 
				
		if(nLevel != 1)
			((MainActivity)context).player.stageCompleteSound();
		if(XmlReader.levelsQuantity(context) < nLevel){
			bExit = true;
		}
		
		if(bExit){
			((Activity)context).runOnUiThread(new Runnable() {
			     @Override
			     public void run() {
			    	 textViewLabel.setVisibility(View.VISIBLE);
			    	 textViewLabel.setText("Congratulations! You won all the levels!");			         
			     }
			});		
			
			try {
				Thread.sleep(3000);
				} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			
			//check for high score
			bCheckHighScoreFinished = false;
			checkHighScore();
			while(!bCheckHighScoreFinished){
				//wait for UI thred..
			}
			//end checking
			bRunning = false;
			((Activity)context).runOnUiThread(new Runnable() {
			     @Override
			     public void run() {
			    	 ((MainActivity)context).setContentView(R.layout.main_menu);			     
			     }
			});
			
			return;
		}
		
		//update lavel
		((Activity)context).runOnUiThread(new Runnable() {
		     @Override
		     public void run() {
		     textViewLevel.setText("Level: " + nLevel);	
		     
		     }
		});		
		
		
		//initialize bricks, paddle and ball
		int nPaddleWidth = XmlReader.getPaddleWidth(nLevel, getContext());
		int nPaddleHeight = XmlReader.getPaddleHeight(nLevel, getContext());
		paddle = new Paddle(this, null, nWidth, nHeight, getContext(), nPaddleWidth, nPaddleHeight);
		
		bricks = new LinkedList<Brick>();
		XmlReader.readLevel(bricks, nLevel, this, this.getContext(), nWidth, nHeight);
		bonuses = new LinkedList<Bonus>();
		
		int nRadius = XmlReader.getBallRadius(nLevel, getContext());
		int nBallPosX = XmlReader.getBallPosition_x(nLevel, getContext()); 
		int nBallPosY = XmlReader.getBallPosition_y(nLevel, getContext());
		int nBallSpeedX = XmlReader.getBallDirection_x(nLevel, getContext());
		int nBallSpeedY = XmlReader.getBallDirection_y(nLevel, getContext());		
		ball = new Ball(paddle, this, null, nWidth, nHeight, getContext(), nRadius, bricks, nBallPosX, nBallPosY, nBallSpeedX, nBallSpeedY, ((MainActivity)context).player, bonuses);		
		
		int nID = R.drawable.background_1 + nLevel - 1;
		Bitmap background = BitmapFactory.decodeResource(getResources(), nID);
		bitmScaledImage = Bitmap.createScaledBitmap(background, nWidth, nHeight, true);
		
		bStarted = false;
		
		Canvas canvas = null;
		try {
			canvas = surfaceHolder.lockCanvas(null);
			synchronized(surfaceHolder) {
				
				drawbackground(canvas);
				paddle.draw(canvas);
				ball.draw(canvas);
				for (int i = 0; i < bricks.size(); i++){
					bricks.get(i).draw(canvas);						
				}								
			}
    	} finally {
    		if (canvas != null) {
    			surfaceHolder.unlockCanvasAndPost(canvas);
    		}
    	}
		Log.d("Android:", "Next Level!!!" );
	}

	@Override
	public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder surfaceHolder) {
	    ((MainActivity)context).player.OKSound();
		this.surfaceHolder = surfaceHolder;
		this.bRunning = true;		
		nWidth = getWidth();
		nHeight = getHeight();
		
		textViewLife =  (TextView) ((Activity)context).findViewById(R.id.textView2);
		textViewLife.setText("Life: "+nPlayerLife);
		textViewLevel = (TextView) ((Activity)context).findViewById(R.id.textView3);
		textViewScore = (TextView) ((Activity)context).findViewById(R.id.textView4);
		textViewScore.setText("Score: " + nScore);
		textViewLabel = (TextView) ((Activity)context).findViewById(R.id.textView5);
		textViewLabel.setVisibility(View.INVISIBLE);
		
		nextLevel();
		
		Thread t = new Thread(this);
		t.start();	
		setUserName();				
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
		// TODO Auto-generated method stub		
	}
	
    @Override
    protected void onDraw(Canvas canvas) {
    	//Draw the Background
    	//canvas.drawColor(Color.BLUE);    	
    	drawbackground(canvas);
    }
    
    private void drawbackground(Canvas canvas){    	
        if(bitmScaledImage!=null)
        	canvas.drawBitmap(bitmScaledImage, 0, 0, null); // draw the background
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
			dEventX = event.getX();
			bTouched = true;
			bStarted = true;
			Log.d("Android:", "touched. bStarted = " + bStarted);
		}
		return bTouched;
	}
}
