package com.unimelb.breakoutgame;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import android.view.ContextThemeWrapper;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedList;

public class XmlReader {
	
	static int getBallRadius(int nLevel, Context context)
	{
		String FILENAME;
		if (nLevel>=1 && nLevel<=levelsQuantity(context))
		{
			FILENAME=getLevelFile(nLevel, context);
		}
		
		else FILENAME=getLevelFile(1, context);
		
		
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier(FILENAME, "xml", context.getPackageName()));
		int ball_radius=0;
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/"+FILENAME+".xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
            
        	if (parser.getEventType() == XmlPullParser.START_TAG
                    && parser.getName().equals("settings")) 
            {
            	ball_radius=Integer.parseInt(parser.getAttributeValue(3));
            }
                        
            parser.next();
        }
        
        
    }
    catch (Throwable t) {
        
    }
		
		return ball_radius;
	}
	
	static int getPaddleWidth(int nLevel, Context context)
	{
		
		String FILENAME;
		if (nLevel>=1 && nLevel<=levelsQuantity(context))
		{
			FILENAME=getLevelFile(nLevel, context);
		}
		
		else FILENAME=getLevelFile(1, context);
		
		
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier(FILENAME, "xml", context.getPackageName()));
		int paddle_width=0;
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/"+FILENAME+".xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
	        
	    	if (parser.getEventType() == XmlPullParser.START_TAG
	                && parser.getName().equals("settings")) 
	        {
	    		paddle_width=Integer.parseInt(parser.getAttributeValue(4));
	        }
	                    
	        parser.next();
	    }
	    
	    
	}
	catch (Throwable t) {}
		
		return paddle_width;
	}
	
	
	
	static int getPaddleHeight(int nLevel, Context context)
	{
		String FILENAME;
		if (nLevel>=1 && nLevel<=levelsQuantity(context))
		{
			FILENAME=getLevelFile(nLevel, context);
		}
		
		else FILENAME=getLevelFile(1, context);
		
		
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier(FILENAME, "xml", context.getPackageName()));
		int paddle_height=0;
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/"+FILENAME+".xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
	        
	    	if (parser.getEventType() == XmlPullParser.START_TAG
	                && parser.getName().equals("settings")) 
	        {
	    		paddle_height=Integer.parseInt(parser.getAttributeValue(5));
	        }
	                    
	        parser.next();
	    }
	    
	    
	}
	catch (Throwable t) {}
		
		return paddle_height;
	}
	
	static String getBackgroundWallpaper(int nLevel, Context context)
	{
		String FILENAME;
		if (nLevel>=1 && nLevel<=levelsQuantity(context))
		{
			FILENAME=getLevelFile(nLevel, context);
		}
		
		else FILENAME=getLevelFile(1, context);
		
		
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier(FILENAME, "xml", context.getPackageName()));
		String Wallpaper="";
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/"+FILENAME+".xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
	        
	    	if (parser.getEventType() == XmlPullParser.START_TAG
	                && parser.getName().equals("settings")) 
	        {
	    		Wallpaper=parser.getAttributeValue(6);
	        }
	                    
	        parser.next();
	    }
	    
	    
	}
	catch (Throwable t) {}
		
		return Wallpaper;
	}
	
	static int getBallDirection_x(int nLevel, Context context)
	{
		String FILENAME;
		if (nLevel>=1 && nLevel<=levelsQuantity(context))
		{
			FILENAME=getLevelFile(nLevel, context);
		}
		
		else FILENAME=getLevelFile(1, context);
		
		
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier(FILENAME, "xml", context.getPackageName()));
		int ballDirection_x=0;
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/"+FILENAME+".xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
	        
	    	if (parser.getEventType() == XmlPullParser.START_TAG
	                && parser.getName().equals("settings")) 
	        {
	    		ballDirection_x=Integer.parseInt(parser.getAttributeValue(7));
	        }
	    	
	        parser.next();
	    }
	    
	    
	}
	catch (Throwable t) {}
		
		return ballDirection_x;
	}
	
	static int getBallDirection_y(int nLevel, Context context)
	{
		String FILENAME;
		if (nLevel>=1 && nLevel<=levelsQuantity(context))
		{
			FILENAME=getLevelFile(nLevel, context);
		}
		
		else FILENAME=getLevelFile(1, context);
		
		
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier(FILENAME, "xml", context.getPackageName()));
		int ballDirection_y=0;
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/"+FILENAME+".xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
	        
	    	if (parser.getEventType() == XmlPullParser.START_TAG
	                && parser.getName().equals("settings")) 
	        {
	    		ballDirection_y=Integer.parseInt(parser.getAttributeValue(8));
	        }
	    	
	        parser.next();
	    }
	    
	    
	}
	catch (Throwable t) {}
		
		return ballDirection_y;
	}

	
	static int getBallPosition_x(int nLevel, Context context)
	{
		String FILENAME;
		if (nLevel>=1 && nLevel<=levelsQuantity(context))
		{
			FILENAME=getLevelFile(nLevel, context);
		}
		
		else FILENAME=getLevelFile(1, context);
		
		
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier(FILENAME, "xml", context.getPackageName()));
		int ballPosition_x=0;
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/"+FILENAME+".xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
	        
	    	if (parser.getEventType() == XmlPullParser.START_TAG
	                && parser.getName().equals("settings")) 
	        {
	    		ballPosition_x=Integer.parseInt(parser.getAttributeValue(9));
	        }
	    	
	        parser.next();
	    }
	    
	    
	}
	catch (Throwable t) {}
		
		return ballPosition_x;
	}
	
	static int getBallPosition_y(int nLevel, Context context)
	{
		String FILENAME;
		if (nLevel>=1 && nLevel<=levelsQuantity(context))
		{
			FILENAME=getLevelFile(nLevel, context);
		}
		
		else FILENAME=getLevelFile(1, context);
		
		
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier(FILENAME, "xml", context.getPackageName()));
		int ballPosition_y=0;
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/"+FILENAME+".xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
	        
	    	if (parser.getEventType() == XmlPullParser.START_TAG
	                && parser.getName().equals("settings")) 
	        {
	    		ballPosition_y=Integer.parseInt(parser.getAttributeValue(10));
	        }
	    	
	        parser.next();
	    }
	    
	    
	}
	catch (Throwable t) {}
		
		return ballPosition_y;
	}
	
	private static String getLevelFile (int Level, Context context)
	{
		String level_name = null;
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier("levels", "xml", context.getPackageName()));
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/levels.xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
            
        	if (parser.getEventType() == XmlPullParser.START_TAG
                    && parser.getName().equals("level") && Integer.parseInt(parser.getAttributeValue(0))==Level) 
            {
            	level_name=parser.getAttributeValue(1);
            }
                        
            parser.next();
        }
        
        
    }
    catch (Throwable t) {
        
    }
		
		return level_name;
	}
	
	private static String readLevelsFile (Context context)
	{
		String fileContents="<levels>";
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/levels.xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
            
        	if (parser.getEventType() == XmlPullParser.START_TAG
                    && parser.getName().equals("level"))
        		
            {
            	fileContents=fileContents+"<level "+parser.getAttributeName(0)+" = \""+ parser.getAttributeValue(0)+"\" "+parser.getAttributeName(1)+" = \""+ parser.getAttributeValue(1)+"\" /> \n";
            }
                        
            parser.next();
        }
                
    }
    catch (Throwable t) {
        
    }
		
		return fileContents;
	}
	
	public static int  levelsQuantity(Context context)
	{
		int max_level=0;
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier("levels", "xml", context.getPackageName()));
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/levels.xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
            
        	if (parser.getEventType() == XmlPullParser.START_TAG
                    && parser.getName().equals("level") && Integer.parseInt(parser.getAttributeValue(0))>max_level) 
            {
            	max_level=Integer.parseInt(parser.getAttributeValue(0));
            }
                        
            parser.next();
        }
        
        
    }
    catch (Throwable t) {
        
    }
		
		return max_level;
	}
	
	public static boolean readLevel (LinkedList<Brick> bricks, int level, WorldView worldview, Context context, int nScreenWidth, int nScreenHeight)
	{
		//ContextThemeWrapper a = new ContextThemeWrapper();
		
		String FILENAME;
		if (level>=1 && level<=levelsQuantity(context))
		{
			FILENAME=getLevelFile(level, context);
		}
		
		else FILENAME=getLevelFile(1, context);
		
		
		//XmlPullParser parser = context.getResources().getXml(R.xml.test_map);
		//XmlPullParser parser = context.getResources().getXml(context.getResources().getIdentifier(FILENAME, "xml", context.getPackageName()));
		
		
		
		
		int b_width=0;
		int b_height=0;
		int position_x=0;
		int position_y=0;
		int type=0;
		int bonus=0;
		
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			File file = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/"+FILENAME+".xml");
			FileInputStream fis = new FileInputStream(file);
			parser.setInput(new InputStreamReader(fis));
		while (parser.getEventType()!= XmlPullParser.END_DOCUMENT) {
            
        	if (parser.getEventType() == XmlPullParser.START_TAG
                    && parser.getName().equals("settings")) 
            {
            	b_width=Integer.parseInt(parser.getAttributeValue(1));
            	b_height=Integer.parseInt(parser.getAttributeValue(2));
            }
            
            if (parser.getEventType() == XmlPullParser.START_TAG
                    && parser.getName().equals("brick")) 
            {
            	position_x=Integer.parseInt(parser.getAttributeValue(0));
            	position_y=Integer.parseInt(parser.getAttributeValue(1));
            	type=Integer.parseInt(parser.getAttributeValue(2));
            	bonus=Integer.parseInt(parser.getAttributeValue(3));


            	bricks.add(new Brick(worldview, nScreenWidth, nScreenHeight, position_x, position_y, b_width, b_height, type, context, bonus));
            	
    			
            	
            }
            
            parser.next();
        }
        
        
    }
    catch (Throwable t) {
        return false;
    }
		/*catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		return true;
	}


public static void addLevelToList(String name, Context context) throws IOException
{
	int lastLevel=levelsQuantity(context);
	lastLevel=lastLevel+1;
	String wrt=readLevelsFile(context);
	wrt=wrt+"<level number=\""+lastLevel+"\" name=\""+name+"\" /> </levels>";
	File fl = new File(Environment.getExternalStorageDirectory().getPath()+"/bricks/levels.xml");
//	if(!fl.exists()){
//		fl.createNewFile();
//	}
//	FileWriter write = new FileWriter(fl.getAbsoluteFile());
//	BufferedWriter bw = new BufferedWriter(write);
//	bw.write(wrt);
//	bw.close();
	//zz.createNewFile();
	FileOutputStream fos = new FileOutputStream(fl);
	OutputStreamWriter outwr=new OutputStreamWriter(fos);
	outwr.append(wrt);
	outwr.flush();
	outwr.close();
	fos.close();
}
}