package com.unimelb.breakoutgame;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;

public class PersonalInformation {

	
	/** Function for getting account number   **/
	  public String getAccountInformation(AccountManager getSystemService) {

	        String accountName = "";
	        Regu_Express express = new Regu_Express();	        
	        AccountManager manager = getSystemService;
	        // get all of accounts from the mobile device. 
	        Account[] list = manager.getAccounts();
	         for(Account account: list) {
	             if (express.validMail(account.name)) {
	            	 accountName = account.name;
	            	 break;
	                 //setContainValue.setTextValue(R.id.text1,account.name.toString());
//	                 TextView t = (TextView)findViewById(R.id.text1);
//	                 t.setText(account.name);


	             }
	         }
			return accountName;

	    }
	  
	  /** Function for getting display name  **/
	  @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH) public String getUserName(ContentResolver contentResolver){
		  String[] projection = new String[] {ContactsContract.Profile.DISPLAY_NAME,ContactsContract.Profile.DISPLAY_NAME_ALTERNATIVE,ContactsContract.Profile.DISPLAY_NAME_PRIMARY,ContactsContract.Profile.DISPLAY_NAME_SOURCE};
		  String displayName = "";
		  
		  Uri dataUri = Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI, ContactsContract.Contacts.Data.CONTENT_DIRECTORY);
		   
		  ///Cursor c = contentResolver.query(dataUri, projection, null, null, null);
		  Cursor c = contentResolver.query(dataUri, projection, null, null, null);
		  try {
		      if (c.moveToFirst()) {
		          // four types display name we can use
		           displayName = c.getString(c.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME));
		           //displayName = c.getString(c.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME_ALTERNATIVE));
		           //displayName = c.getString(c.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME_PRIMARY));
		          // displayName = c.getString(c.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME_SOURCE));
		      }
		  } finally {
		      c.close();
		  }
		  return displayName;
	  }
}
