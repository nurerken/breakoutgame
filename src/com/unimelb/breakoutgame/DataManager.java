package com.unimelb.breakoutgame;

import android.app.Application;

public class DataManager  extends Application{
	private static DataManager instance;
	private String account;
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public static DataManager getInstance() {
		if (instance == null)
			instance = new DataManager();
		return instance;
	}
}
