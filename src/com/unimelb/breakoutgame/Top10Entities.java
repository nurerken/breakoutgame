package com.unimelb.breakoutgame;

import java.util.List;

public class Top10Entities extends entities{
    private String accountName ;
	private int level;
    private int score;
    private int order;
    private String displayName;
    


	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	@Override
	public List<entities> returnEntities() {
		// TODO Auto-generated method stub
		return super.returnEntities();
	}

	@Override
	public void setEntities(entities data) {
		// TODO Auto-generated method stub
		super.setEntities(data);
	}
	
	
}