package com.unimelb.breakoutgame;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;

public class Baidu extends AsyncTask<String, Integer, String> {

	private Context context;
    private PowerManager.WakeLock mWakeLock;
    private String levelFileName;
    public String getLevelFileName() {
		return levelFileName;
	}

	public void setLevelFileName(String levelFileName) {
		this.levelFileName = levelFileName;
	}

	public Baidu(Context context) {
        this.context = context;
    }

	@Override
	protected String doInBackground(String... sUrl) {
		// TODO Auto-generated method stub
		InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(sUrl[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = connection.getInputStream();
           // create a File object for the parent directory
            String a = Environment.getExternalStorageDirectory().getPath(); 
            File wallpaperDirectory = new File("/"+Environment.getExternalStorageDirectory().getPath() +"/bricks/");
            // have the object build the directory structure, if needed.
            wallpaperDirectory.mkdirs();
            output = new FileOutputStream("/"+Environment.getExternalStorageDirectory().getPath() +"/bricks/"+levelFileName);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
	}
	
	public void CreateFilePath(){
		File wallpaperDirectory = new File("/"+Environment.getExternalStorageDirectory().getPath() +"/bricks/");
        // have the object build the directory structure, if needed.
        wallpaperDirectory.mkdirs();
	}
}
