package com.unimelb.breakoutgame;

import com.unimelb.bouncyball.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.transition.Scene;

public class Paddle {
	public int nWidth;
	public int nHeight;
	private int nScreenWidth;
	private int nScreenHeight;
	public float dCenter;
	private WorldView worldView;
	private Bitmap bmp;
	Paint paint;
	Rect rec;
	Context context;
	Bitmap bitmScaledImage;
	Bitmap image;
	
	public Paddle(WorldView worldView, Bitmap bmp, int screenWidth, int screenHeight, Context context, int width, int height){
		this.nScreenHeight = screenHeight;
		this.nScreenWidth = screenWidth;
		this.nWidth = screenWidth * width/100;
		this.nHeight = screenHeight * height/100;
		this.worldView = worldView;
		this.context = context;
		
		this.bmp = bmp;
		paint = new Paint();
    	paint.setAntiAlias(true);
    	paint.setColor(Color.BLUE);
    	
    	rec = new Rect();
		dCenter = (screenWidth / 2);
		rec.bottom = nScreenHeight;
		rec.top = rec.bottom - this.nHeight;
		rec.left = nScreenWidth / 2 - this.nWidth / 2;
		rec.right = rec.left + nWidth;	
		
		image = BitmapFactory.decodeResource(context.getResources(), R.drawable.paddle);
		bitmScaledImage = Bitmap.createScaledBitmap(image, rec.width(), rec.height(), true);
	}
	
	public void move(int nCenter){
		dCenter = nCenter;		
		rec.left = nCenter - nWidth / 2;
		rec.right = rec.left + nWidth;
	}
	
	public void IncreasePaddleWidth(){
		nWidth += nWidth*0.5;
		rec.left = nScreenWidth / 2 - this.nWidth / 2;
		rec.right = rec.left + nWidth;
		bitmScaledImage = Bitmap.createScaledBitmap(image, rec.width(), rec.height(), true);
	}
	
	public void draw(Canvas canvas) {    	
    	if(worldView.bOnScreen) {
    		//canvas.drawRect(rec, paint);
    		canvas.drawBitmap(bitmScaledImage, rec.left, rec.top, null); 
    	}
    }
	
}
