Group ID: 28.
This is a default breakout game described in the LMS by. We implemented all the basic requirements. Moreover,
we added sound effect to the game as extra bonus.

Nurlan and Arjun were responsible for game engine. This includes funcionalities:
-drawing scene, ball, paddle, bricks
-showing information about player name, level, score, life
-moving ball and its reflections from the borders of the screen and collision with bricks and paddle
-bonuses. some bricks give extra life for the player or increases paddle width or ball speed. 
 There are 3 types of bricks. Some of them have extra bonuses which can be either player life increase or 
 by randomly it increases paddle width by 1.5 times or ball speed.
-moving to next level, game over detection. Checking for high score when game over or all the levels completed and updating
 high score with relevant feedback to the player (by using progressbar and labels)

These functionalities are implemented in files:
WorldView.java - main game engine class
Additional classes:
Ball.Java
Brick.Java
Bonus.java
Paddle.java


Jacky:
...

Tigran:
..